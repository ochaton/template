# Tarantool template app
Basic template for tarantool application

## File Structure
```
├── .gitlab-ci.yml
├── .luacheckrc                   # configuration of linter
├── .luarocks                     # configuration of luarocks 3.4.0
│   ├── config-5.1.lua            # config to build application
│   └── default-lua-version.lua
├── .tarantoolctl                 # configuration of .tarantoolctl (local development)
├── Dockerfile                    # Dockerfile is used to build rpm package
├── Makefile                      # Makefile is helper file to run builder and get dependencies
├── README.md
├── etc
│   ├── conf.inst.lua             # use this config as a template for production
│   ├── conf.lua                  # this config is for local development
│   └── switchover.yaml           # config of switchover (https://gitlab.com/ochaton/switchover)
├── init.lua                      # Entrypoint of Tarantool (will be deployed to production)
├── lua_modules                   # Directory of luarocks project
├── myapp                         # Root of the application
│   ├── init.lua
│   └── migrations                # directory of DDL migration (ruled by https://github.com/igorcoding/tarantool-spacer)
├── myapp_001.lua -> init.lua     # link to single instance
├── rockspecs
│   └── myapp-scm-1.rockspec      # Rockspec of the project (can be updated by tarantool tools/refresh_rockspec myapp)
├── rpm
│   └── myapp.spec                # RPM spec for CentOS
└── tools
    └── refresh_rockspec.lua      # Script which rebuilds rockspec of application
```


## Local development

### Preparations
1. Install Tarantool: https://www.tarantool.io/en/download/
2. Install Luarocks 3.0+: https://luarocks.org/ (using make install, yes)
3. Install docker (to build your application)
4. Install make, gcc, cmake, and other build tools (build-essential)

### Run your application
1. Run `make dep` to download dependencies
2. Run `tarantoolctl start myapp_001` -> to run tarantool in background
3. Run `tarantoolctl enter myapp_001` -> to enter into tarantool console
4. Run `package.reload()` -> to refresh code (or `tarantoolctl restart myapp_001`)
5. You may read logs of tarantool inside .tnt/myapp_001.log
6. Use `etc/conf.lua` to configurate tarantool for local development

## Build rpm package
Just run `make rpm` and fetch your RPM from RPMS/x86_64 directory
