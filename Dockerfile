FROM ochaton/luarocks:5.1

RUN yum install -y rpm-build tarantool tarantool-devel
ADD . /source

WORKDIR /source
RUN eval "$(luarocks path)" && rpmbuild --define "SRC_DIR $(pwd)" -ba rpm/*.spec