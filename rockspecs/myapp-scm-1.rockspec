package = "myapp"
version = "scm-1"
source = {
   url = "git+https://github.com/moonlibs/baselayout.git"
}
description = {
   homepage = "https://github.com/moonlibs/baselayout",
   license = "Proprietary"
}
dependencies = {
   "kit scm-2",
   "config scm-4",
   "package-reload scm-1",
   "ctx",
   "val",
   "id",
   "moonwalker scm-1",
   "ffi-reloadable scm-1",
   "spacer scm-3",
   "xqueue scm-5",
   "metrics 0.9.0",
   "http 1.1.0"
}
build = {
   type = "builtin",
   modules = {
      myapp = "myapp/init.lua"
   },
   copy_directories = {},
   install = {
      bin = {
         myapp = "init.lua"
      }
   }
}
