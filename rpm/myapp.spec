%define version 1.0

%define release %(/bin/date +"%Y.%m%d.%H%M")
%define packagename myapp

Name:           %{packagename}
Version:        %{version}
Release:        %{release}
Summary:        "Tarantool application"

Group:          tarantool/db
License:        proprietary

%if %{?SRC_DIR:0}%{!?SRC_DIR:1}
Source0: %{packagename}.tar.bz2
%endif

Requires: tarantool >= 2.7.2
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot

%description
%{__summary}

%if %{?SRC_DIR:1}%{!?SRC_DIR:0}
Branch: %(git rev-parse --abbrev-ref HEAD)
Commit: %(git rev-parse HEAD)
%define __git_branch %(git rev-parse --abbrev-ref HEAD)
%define __git_commit %(git rev-parse HEAD)
$ git status -suno
%(git status -suno)
%endif

%prep
pwd
%if %{?SRC_DIR:1}%{!?SRC_DIR:0}
	rm -rf %{packagename}
	cp -r %{SRC_DIR} %{packagename}
	cd %{packagename}
%else
%setup -q -n %{packagename}
%endif

%build
pwd
cd %{packagename}

ln -s /source/.luarocks ~/
rm -rvf lua_modules
tarantool tools/refresh_rockspec.lua %{packagename}
luarocks make rockspecs/%{packagename}-scm-1.rockspec

%install
pwd
cd %{packagename}

[ "%{buildroot}" != "/" ] && rm -rf %{buildroot}

find /build/usr/lib/luarocks/rocks-5.1 -type f | grep -v "/%{packagename}/scm-1/" | xargs -n1 -I {} rm -rvf {}
find /build/usr/lib/luarocks -type d -empty -print
find /build/usr/lib/luarocks -type d -empty -delete
cp -vaR /build %{buildroot}

%clean
pwd
rm -rf %{buildroot}

%files
%defattr(-,root,root)
/usr/bin/%{packagename}
/usr/local/lib64/tarantool/
/usr/local/share/tarantool/
/usr/lib/luarocks/rocks-5.1/%{packagename}/scm-1/

%changelog
