assert(instance_name, "instance_name is required from symlink")

etcd = {
	endpoints = {
		"http://etcd0:2379",
		"http://etcd1:2379",
		"http://etcd2:2379",
	},
	timeout = 1,
	prefix = "/myapp/",
	instance_name = instance_name,
	boolean_auto = true,
	integer_auto = true,
}

box = {
	pid_file                = "/var/run/tarantool/"..instance_name..".pid",
	work_dir                = "/var/lib/tarantool/"..instance_name,
	memtx_dir               = "/var/lib/tarantool/snaps/"..instance_name,
	wal_dir                 = "/var/lib/tarantool/xlogs/"..instance_name,
}

app = {
}