-- List of rocks servers:
rocks_servers = {
	"https://rocks.ochaton.me",
	"http://moonlibs.github.io/rocks", -- moonlibs libs
	"http://rocks.tarantool.org/", -- tarantool libs
	"http://luarocks.org/repositories/rocks", -- luarocks
}

wrap_bin_scripts = false

-- Configuration of luarocks:
rocks_trees = {
	{
		root = "/build/usr",
		bin_dir = "/build/usr/local/bin",
		lib_dir = "/build/usr/local/lib64/tarantool/",
		lua_dir = "/build/usr/local/share/tarantool/",
	}
}