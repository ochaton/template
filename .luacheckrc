std = "tarantool"

codes = true
max_line_length = false

include_files = { "myapp", "init.lua" }

read_globals = {
	"package.reload",
	"myapp",
	"spacer",
	"config",
}

unused_args = false