ROOT_DIR:=$(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))
BRANCH := $(shell git rev-parse --abbrev-ref HEAD)
VERSION := $(shell git describe --long | sed -e 's/-/./g' -e 's/\.[^\.]*$$//g')
APPNAME := myapp

.PHONY: all pack dep rpm

rpm:
	docker build -t myapp-tarantool .
	docker cp $$(docker run -d myapp-tarantool):/root/rpmbuild/RPMS/ ./

dep:
	luarocks --lua-version 5.1 --tree=.rocks install --only-deps rockspecs/${APPNAME}-scm-1.rockspec