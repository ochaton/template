#!/usr/bin/env tarantool

require'strict'.on()
local fiber = require 'fiber'
local under_tarantoolctl = fiber.name() == 'tarantoolctl'

local log = require('log')
local fio = require('fio');
local luaroot = debug.getinfo(1,"S")
local source  = fio.abspath(luaroot.source:match('^@(.+)'));
local symlink = fio.readlink(source);
local script_path = (symlink and fio.abspath(symlink) or source):match("^(.*/)")
local instance_name = (function()
	if symlink then
		return (source:match("/([^/]+)$"):gsub('%.lua$',''))
	else
		return (source:gsub('/init%.lua$',''):match('/([^/]+)$'))
	end
end)()
local appname = instance_name:match("(%S+)_%d+$")
rawset(_G,'who',string.format("%s#%s",appname,instance_name))

log.info("Starting app %s, instance %s", appname, instance_name)

local function single_config_path()
	local env_conf = os.getenv('CONF')
	if env_conf then return env_conf end

	if ( symlink and source:match('^/etc/') ) or source:match('^/usr/') then
		-- system wide. /etc/{appname}/conf.lua
		return string.format("/etc/%s/conf.lua", appname)
	else
		-- local user
		return string.format("%s/etc/conf.lua", script_path)
	end
end

local function addpaths(dst,...) local cwd = script_path; local pp = {}; for s in package[dst]:gmatch("([^;]+)") do pp[s] = 1 end; local add = ''; for _, p in ipairs({...}) do if not pp[cwd..p] then add = add..cwd..p..';'; end end;package[dst]=add..package[dst];return end
addpaths('path', './?.lua', './?/init.lua', '.rocks/share/lua/5.1/?.lua', '.rocks/share/lua/5.1/?/init.lua')
addpaths('cpath', '.rocks/lib/lua/5.1/?.so', '.rocks/lib/lua/?.so', '.rocks/lib64/lua/5.1/?.so')

require 'package.reload'
require 'kit'

require 'config' {
	instance_name = instance_name,
	file          = single_config_path(),
	on_load       = function(_,cfg)
		if cfg.box.background ~= nil and not cfg.box.background and under_tarantoolctl then
			cfg.box.background = true
		end
	end;
	mkdir         = true,
	master_selection_policy = 'etcd.cluster.master',
}
box.once('access:v1', function()
	box.schema.user.grant('guest', 'read,write,execute', 'universe', nil, {if_not_exists=true})
end)

local appname_path = fio.dirname(package.search(appname))
log.info("Application located at: %s", appname_path)

local spacer = require 'spacer'.new {
	migrations = fio.pathjoin(appname_path, 'migrations'),
	global_ft = false,
}
rawset(_G, 'spacer', spacer)

box.once('schema:v1', function() spacer:migrate_up() end)

local app = require(appname)
rawset(_G, appname, app)

if type(app.start) == 'function' then
	app.start(config)
end

if package.reload.count == 1 and not fiber.self().storage.console and not under_tarantoolctl then
	require 'console'.start()
	os.exit(0)
end